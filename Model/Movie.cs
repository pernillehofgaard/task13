﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class Movie
    {
        public int Id { get; set; } //PK
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Pic { get; set; }
        public string Trailer { get; set; } //Youtube link

        //Nav property
        public Franchise Franchise { get; set; }
        public int FranchiseId { get; set; } //FK
        public ICollection<MovieCharacter> movieCharacters { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
