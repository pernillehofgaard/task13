﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class DbMovieContext : DbContext
    {
        public DbSet<Actor> Actor { get; set; }
        public DbSet<Character> Character { get; set; }
        public DbSet<MovieCharacter> MovieCharacter { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            optionBuilder.UseSqlServer("Data Source=PC7370\\SQLEXPRESS;Initial Catalog=MovieDatabase; Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Adding PK/FK in Linking table
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new {mc.MovieId, mc.ActorId, mc.CharacterId });

            //adding dummy data to Actor
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 1, FirstName = "Uma" , LastName = "Thurman", DOB = new DateTime(1970,04,26), Gender = "Female", POB = "USA"});
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 2, FirstName = "Robert" , MiddleName = "Jr", LastName = "Downey", DOB = new DateTime(1965,04,04), Gender = "Male", POB = "USA"});
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 3, FirstName = "Tom" , LastName = "Holland", DOB = new DateTime(1996,06,01), Gender = "Male", POB = "England"});
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 4, FirstName = "John" , LastName = "Travolta", DOB = new DateTime(1954,02,18), Gender = "Male", POB = "USA"});
            modelBuilder.Entity<Actor>().HasData(new Actor { Id = 5, FirstName = "Samuel" , MiddleName = "Leroy", LastName = "Jackson", DOB = new DateTime(1948,12,21), Gender = "Male", POB = "USA", Pic = "Link"});

            //adding dummy data to Character
            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Tony Stark", Alias = "Iron Man", Gender = "Male"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FullName = "Beatrix Kiddo", Alias = "Beep", Gender = "Female"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FullName = "Peter Parker", Alias = "Spider Man", Gender = "Male"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FullName = "Vincent Vega", Alias = "Vincent", Gender = "Male"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 5, FullName = "Jules Winfield", Alias = "Jules", Gender = "Male"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 6, FullName = "Danny Zuko", Alias = "Danny", Gender = "Male"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 7, FullName = "Major Marquis Warren", Alias = "Warren", Gender = "Male"}); //jackson

            //adding dummy data to Franchise
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Marvel univers", Description = "World of the best super heroes"});
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "DC univers", Description = "World of the next best super heroes"});
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Taratino Univers", Description = "Gangsters and shit"});
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 4, Name = "Greace univers", Description = "Rom-Com-Musical"});

            //adding dummy data to movie
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, MovieTitle = "Civil war", Genre = "action", Director = "DirectorMarvel", Pic = "MarvelPic", Trailer = "CivilwarTrailer", FranchiseId = 1});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, MovieTitle = "End game", Genre = "action", Director = "DirectorMarvel", Pic = "MarvelPic2", Trailer = "EndTrailer", FranchiseId = 1});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, MovieTitle = "Aqua man", Genre = "action", Director = "DirectorDC", Pic = "AquaManPic", Trailer = "AquaManTrailer", FranchiseId = 2});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 4, MovieTitle = "Pulpfiction", Genre = "action", Director = "Quentin Tarantino", Pic = "UmaThurmanSmoking", Trailer = "PulpfictionTrailer", FranchiseId = 3});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 5, MovieTitle = "Greace", Genre = "Musical", Director = "DirectorGreace", Pic = "GreacePic", Trailer = "GreaceTrailer", FranchiseId = 4});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 6, MovieTitle = "Kill bill", Genre = "action", Director = "Quentin Tarantino", Pic = "killBillPic", Trailer = "KillBillTrailer", FranchiseId = 3});
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 7, MovieTitle = "The hateful eight", Genre = "action", Director = "Quentin Tarantino", Pic = "Hateful8Pic", Trailer = "Hateful8Trailer", FranchiseId = 3});

            //adding dummy data to MovieCharacter
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter {ActorId = 2, CharacterId = 1, MovieId = 1}); //downey as ironman in civil war
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter {ActorId = 2, CharacterId = 1, MovieId = 2});//downy as ironman in endgame
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter {ActorId = 3, CharacterId = 3, MovieId = 2});//holland as spiderman in endgame
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter {ActorId = 5, CharacterId = 7, MovieId = 7}); //jackson as warren in hatefull8
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter {ActorId = 4, CharacterId = 6, MovieId = 5}); //travolta as danny in greace
            modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter {ActorId = 4, CharacterId = 4, MovieId = 4}); //travolta as vincent in pulpfiction
            
            }
    }
}
