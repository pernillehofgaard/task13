﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class Actor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOB { get; set; }
        public string POB { get; set; }
        public string Pic { get; set; }

        //Nav
        public ICollection<MovieCharacter> movieCharacters { get; set; } //FK

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
