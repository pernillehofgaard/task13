﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Task13
{
    class MovieCharacter
    {
        public int ActorId { get; set; } //FK
        public int MovieId { get; set; } //FK
        public int CharacterId { get; set; } //FK

        //Nav properties
        public Actor actor { get; set; }
        public Movie movie { get; set; }
        public Character character { get; set; }


    }
}
