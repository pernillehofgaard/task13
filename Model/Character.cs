﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class Character
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Pic { get; set; }

        public ICollection<MovieCharacter> MovieCharacter { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
