﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class Franchise
    {
        public int Id { get; set; } //PK
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Movie> movies { get; set; } //FK
    }
}
