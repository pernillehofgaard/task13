﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task13.Migrations
{
    public partial class AddedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    POB = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    Director = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    ActorId = table.Column<int>(nullable: false),
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.ActorId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Actor_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actor",
                columns: new[] { "Id", "DOB", "FirstName", "Gender", "LastName", "MiddleName", "POB", "Pic" },
                values: new object[,]
                {
                    { 1, new DateTime(1970, 4, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Uma", "Female", "Thurman", null, "USA", null },
                    { 2, new DateTime(1965, 4, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Robert", "Male", "Downey", "Jr", "USA", null },
                    { 3, new DateTime(1996, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tom", "Male", "Holland", null, "England", null },
                    { 4, new DateTime(1954, 2, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", "Male", "Travolta", null, "USA", null },
                    { 5, new DateTime(1948, 12, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Samuel", "Male", "Jackson", "Leroy", "USA", "Link" }
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Pic" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Tony Stark", "Male", null },
                    { 2, "Beep", "Beatrix Kiddo", "Female", null },
                    { 3, "Spider Man", "Peter Parker", "Male", null },
                    { 4, "Vincent", "Vincent Vega", "Male", null },
                    { 5, "Jules", "Jules Winfield", "Male", null },
                    { 6, "Danny", "Danny Zuko", "Male", null },
                    { 7, "Warren", "Major Marquis Warren", "Male", null }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "World of the best super heroes", "Marvel univers" },
                    { 2, "World of the next best super heroes", "DC univers" },
                    { 3, "Gangsters and shit", "Taratino Univers" },
                    { 4, "Rom-Com-Musical", "Greace univers" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Pic", "Trailer" },
                values: new object[,]
                {
                    { 1, "DirectorMarvel", 1, "action", "Civil war", "MarvelPic", "CivilwarTrailer" },
                    { 2, "DirectorMarvel", 1, "action", "End game", "MarvelPic2", "EndTrailer" },
                    { 3, "DirectorDC", 2, "action", "Aqua man", "AquaManPic", "AquaManTrailer" },
                    { 4, "Quentin Tarantino", 3, "action", "Pulpfiction", "UmaThurmanSmoking", "PulpfictionTrailer" },
                    { 6, "Quentin Tarantino", 3, "action", "Kill bill", "killBillPic", "KillBillTrailer" },
                    { 7, "Quentin Tarantino", 3, "action", "The hateful eight", "Hateful8Pic", "Hateful8Trailer" },
                    { 5, "DirectorGreace", 4, "Musical", "Greace", "GreacePic", "GreaceTrailer" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieId", "ActorId", "CharacterId" },
                values: new object[,]
                {
                    { 1, 2, 1 },
                    { 2, 2, 1 },
                    { 2, 3, 3 },
                    { 4, 4, 4 },
                    { 7, 5, 7 },
                    { 5, 4, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_ActorId",
                table: "MovieCharacter",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Actor");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
